---
layout: post
title:  "Introducing Ozone prediction project!"
date:   2018-10-27 15:32:14 -0300
author: "Daniel Klamt"
categories: jurnal
tags: [documentation,introduction]
image: sky.jpg
---

In the last few years, there is growing public anxiety over levels of air
pollution in our cities. On average, a person inhales about 14.000 liters of
air every day, and the presence of contaminants in this air can adversely affect
people's health. Nowadays many experts have found dierent correlations between
health disease and the air quality which population breathe. The purpose of
this project is to analyze this huge problem focusing on the Ozone.

Usually, we think of Ozone as a benecial gas that preserves life absorbing
harmful ultraviolet radiation from the sun in the upper atmosphere. Things
change drastically when Ozone is formed in the lower atmosphere by chemical
reactions.  Our idea was to concentrate in a specic area, at least at the
beginning, and study the distribution of the Ozone during the previous years,
in dierent seasons, and predict its concentration for the future.

Our goal in this project is to predict Ozone levels in Kumpula's area. We want
to aware people about the role of Ozone in the earth and how it is going to
aect citizen's health in the close future, so that we can start taking
decisions now.

On this [Website][jekyll-main] we publish the results of our work, for
everybody to read.  On this Website you can learn about what Ozone is and where
Ozone is good and where it is harmfull.  We also describe how the Ozone level
is affected by other Gases, the Month of the year and the Temperature.

For the People more interested in the Datascience side of the Project we describe the Models we used and how accurate the predition is.
Furthermore you can download the whole project from our [git repository][jekyll-git].

[jekyll-main]: https://ozoneprediction.gitlab.io
[jekyll-git]: https://gitlab.com/ozonePrediction/datasciencegroupproject
