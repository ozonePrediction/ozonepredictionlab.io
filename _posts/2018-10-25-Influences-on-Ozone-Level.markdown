---
layout: post
title: "Influences on the Ozone Level!"
date:   2018-10-25 17:38:13 -0300
author: "Daniel Klamt"
categories: jurnal
tags: [documentation,introduction]
image: pollution.jpg
---

## When is Ozone bad
Bad ozone is created because of the chemical reaction between Oxides of
Nitrogen and volatile organic compounds, when the sunlight is present. One of
the major sources of NOx and VOC are  motor vehicle exhaust, gasoline vapors,
chemical etc. There are some health's problems regarded to the ozone level.

![alt text](https://www.niwa.co.nz/sites/niwa.co.nz/files/styles/large/public/512px-Atmospheric_Layers.svg_.png?itok=HUgcKNKQ  "Athmospheric layers")
Ozone can make more difficult to breathe deeply and vigorously, as well as
bronchitis or asma. Furthermore, Ozone affects sensitive vegetation and
ecosystems, including forests, parks, wildlife refuges and wilderness areas. In
particular, ozone harms sensitive vegetation during the growing season [(Amos et al, 2014).](https://www.nature.com/articles/nclimate2317)
Ozone pollution is more relevant during the summer. Due to sunlight and hot weather it results in harmful ozone concentrations in the stratosphere.

## Which features correlate with the Ozone level
### Selection of features
While generating the SVM Model we evaluated the the different features to see which ones are good indicators for the Ozone level.
To do so we generated the modell with the different features.
This function had an max at eight this eight most importent features.
![alt text](https://gitlab.com/ozonePrediction/datasciencegroupproject/raw/master/results/images/selection.png "Feature selection")
![alt text](https://gitlab.com/ozonePrediction/datasciencegroupproject/raw/master/results/images/features_importance.png  "Feature importance")
This graph shows the increase of the quality, by adding the features.

### Description of correlation and Feature:

In the following part this article will describe the features selected above,
which have the biggest correlation with the Ozone level.

#### Date:
![alt text](https://gitlab.com/ozonePrediction/datasciencegroupproject/raw/master/results/images/total_values.png "Ozone level over Time")
As you can see in the graph the Ozone level has a periodic aspect repeating
every Year.  From this graph and the fact that the month is a good indicator
for the Ozone level as explained above, we can take that there are some aspects
influencing the Ozone level that appear depending on the time of the year.

#### Temperature:
In our project data analysis we can see that there is a strong Ozone
temperature relationship. Higher temperatures caused by increasing greenhouse
gas concentrations are predicted to exacerbate photochemical smog if precursor
emissions remain constant (Bryan et al ,2009) .  Photochemical smog is a type
of smog produced when ultraviolet light from the sun reacts with nitrogen
oxides in the atmosphere.
[(Energyeducation.ca)](https://energyeducation.ca/encyclopedia/Photochemical_smog)

#### Carbon monoxide ( CO ) :
is a colorless, odorless, and tasteless gas that is slightly less dense than air.
CO is found in fumes produced any time you burn fuel in cars or trucks, small engines, stoves, lanterns,
grills, replaces, gas ranges, or furnaces. CO can build up indoors and poison people and animals who
breathe it.

#### Nitrogen monoxide ( N O ) :
The level of no has a very hight correlation with the level of Ozone.
It is an toxic gas formed in many reactions in which nitric acid is reduced. It reacts
immediately with oxygen to form nitrogen dioxide.

#### Nitrogen oxides ( N O x ) :
Nox is the level of no and no2 combined.
