---
layout: page
title: Contact
---

If you are having any problems, any questions or suggestions, feel free to [mail to me](eleon@ele0n.de), or [file a GitHub issue](https://gitlab.com/ozonePrediction/datasciencegroupproject/issues)
