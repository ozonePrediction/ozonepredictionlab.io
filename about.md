---
layout: page
title: About
permalink: /about/
---

This is the Website of the Ozone prediction Project which was created during
the Datascience Course at the University of Helsinki Finland.  This Project
takes a look at how the Ozone level in the Region of Helsinki changes dependent
on different Factors.

You can find the source code for the Ozone prediction project at:
[Gitlab](https://gitlab.com/ozonePrediction/datasciencegroupproject)

